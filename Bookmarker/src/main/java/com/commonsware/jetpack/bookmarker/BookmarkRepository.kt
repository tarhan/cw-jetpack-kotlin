package com.commonsware.jetpack.bookmarker

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jsoup.Jsoup
import java.net.URI

object BookmarkRepository {
  suspend fun save(context: Context, pageUrl: String) =
    withContext(Dispatchers.IO) {
      val db: BookmarkDatabase = BookmarkDatabase[context]
      val entity = BookmarkEntity()
      val doc = Jsoup.connect(pageUrl).get()

      entity.pageUrl = pageUrl
      entity.title = doc.title()

      // based on https://www.mkyong.com/java/jsoup-get-favicon-from-html-page/

      val iconUrl: String? =
        doc.head().select("link[href~=.*\\.(ico|png)]").first()?.attr("href")
          ?: doc.head().select("meta[itemprop=image]").first().attr("content")

      if (iconUrl != null) {
        val uri = URI(pageUrl)

        entity.iconUrl = uri.resolve(iconUrl).toString()
      }

      db.bookmarkStore().save(entity)

      BookmarkModel(entity)
    }

  fun load(context: Context): LiveData<List<BookmarkModel>> {
    val db: BookmarkDatabase = BookmarkDatabase[context]

    return Transformations.map(db.bookmarkStore().all()) { entities ->
      entities.map { BookmarkModel(it) }
    }
  }
}
