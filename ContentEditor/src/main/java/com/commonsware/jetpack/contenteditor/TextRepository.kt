/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Android Jetpack_

  https://commonsware.com/Jetpack
*/

package com.commonsware.jetpack.contenteditor

import android.content.ContentResolver
import android.content.Context
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Environment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.FileNotFoundException
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.Charset


object TextRepository {
  suspend fun read(context: Context, source: Uri) = withContext(Dispatchers.IO) {
    val resolver: ContentResolver = context.contentResolver

    try {
      resolver.openInputStream(source)?.use { stream ->
        StreamResult.Content(source, stream.readText())
      } ?: throw IllegalStateException("could not open $source")
    } catch (e: FileNotFoundException) {
      StreamResult.Content(source, "")
    } catch (t: Throwable) {
      StreamResult.Error(t)
    }
  }

  suspend fun write(context: Context, source: Uri, text: String) = withContext(Dispatchers.IO) {
    val resolver: ContentResolver = context.contentResolver

    try {
      resolver.openOutputStream(source)?.use { stream ->
        stream.writeText(text)

        val externalRoot =
          Environment.getExternalStorageDirectory().absolutePath

        if (source.scheme == "file" && source.path!!.startsWith(externalRoot)) {
          MediaScannerConnection
            .scanFile(
              context,
              arrayOf(source.path),
              arrayOf("text/plain"),
              null
            )
        }

        StreamResult.Content(source, text)
      } ?: throw IllegalStateException("could not open $source")
    } catch (t: Throwable) {
      StreamResult.Error(t)
    }
  }
}

private fun InputStream.readText(charset: Charset = Charsets.UTF_8): String =
  readBytes().toString(charset)

private fun OutputStream.writeText(
  text: String,
  charset: Charset = Charsets.UTF_8
): Unit = write(text.toByteArray(charset))
